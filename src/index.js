'use strict';

const transformer = require('xana-transformer');

/**
 * Return a prev-middleware that transforms coming requests.
 * @param schema
 * @returns {Function}
 */
function transformReq(schema) {
  const transform = transformer.create(schema);
  return (req, next) => transform(req, (err) => {
    next(err, req);
  });
}

module.exports = transformReq;
